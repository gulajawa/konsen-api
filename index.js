const restify = require('restify');
const config = require('./config');

process.env.TZ = config.timeZone;

var server = restify.createServer({
    name: 'Trash Rescue API',
    version: '1.0.0'
});

var io = require("socket.io")(server.server);

function corsHandler(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET,DELETE,POST,PUT,DELETE,OPTIONS');
    res.setHeader('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
    res.setHeader('Access-Control-Max-Age', '1000');
    return next();
}

function optionsRoute(req, res, next) {
    res.send(200);
    return next();
}

server.use(restify.CORS());
server.use(restify.fullResponse());

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.opts('/\.*/', corsHandler, optionsRoute);

server.listen(process.env.PORT || config.port,function (err) {
    if(err){
        console.log('error:',err.message);
        process.exit(1);
    }
    console.log('server listening on port',process.env.PORT || config.port);
    require('./app/core/mongoose')(server,io);
});


process.on('uncaughtException', function(err){
    if(err){
        console.log('lagi error nih \n',err);
    }
});


