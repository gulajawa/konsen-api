/**
 * Created by muharizals on 07/04/2017.
 */

module.exports =  {
    PARAMETER_REQUIRED : "PARAMETER_REQUIRED",
    UNKNOWN_ERROR : "UNKNOWN_ERROR",

    INVALID_NUMBER: "INVALID_NUMBER"
};
