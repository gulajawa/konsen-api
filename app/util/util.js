/**
 * Created by muharizals on 07/04/2017.
 */
const  util = require('util');

exports.validateParameter=function (req,args) {
    let status = true;
    for (let i=0;i<args.length;i++) {
        status = req.params.hasOwnProperty(args[i]);
        if (!status) {
            break;
        }
    }
    return status;
};

exports.isEmpty=function(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
};

exports.randomInt=function(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
};

exports.cekNumber = function (req) {
    return util.isNumber(req);
};