/**
 * Created by muharizals on 07/04/2017.
 */

const util = require('util');

exports.responseHelper =function (res,iserror,code,data,rescode) {
    let opt = {
        error:iserror
    };
    if(rescode && util.isNumber(rescode)){
        res.status(rescode);
    }
    if(iserror && (code && util.isString(code))){
        opt.code = code;
    }

    if(!iserror && data){
        opt.data = data;
    }

    if(iserror && data){
        opt.message = data;
    }
    return res.send(opt);
};