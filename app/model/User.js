/**
 * Created by muharizals on 07/04/2017.
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    uniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    fbid:{type:String,unique:true},
    name:String,
    email:{type:String,unique:true,sparse: true},
    point:Number,
    picture:String
},{timestamps:Date});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User',userSchema);