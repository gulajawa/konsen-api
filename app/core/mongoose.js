/**
 * Created by muharizals on 07/04/2017.
 */
const mongoose = require('mongoose');
const config = require('../../config');

module.exports =  function(server,io){
    mongoose.Promise = global.Promise;
    mongoose.connect(config.dbPath,{server:{auto_reconnect:true}});
    var db = mongoose.connection;

    db.on('error',function (err) {
        console.log('MongoDB Error ');
        console.log(err.message);
        process.exit(1);
    });
    db.on('open',function callback() {
        console.log('MongoDB is ready..');
        require('../router')(server,io);
    });
    db.on('disconnected',function(){
        console.log('MongoDB disconnected.');
        mongoose.connect(config.dbPath,{server:{auto_reconnect:true}});
    });
    db.on('reconnected',function(){
        console.log('mongo db on reconnected');
        require('../routes')(server,io);
    });

};
