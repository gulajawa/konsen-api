/**
 * Created by muharizals on 07/04/2017.
 */
/**
 * Created by muharizals on 02/02/2017.
 */

const userController = require('../controller/UserController');

module.exports =function (app,io) {

    userController.initSocketIo(io);

    app.get('/',function (req,res) {
        return res.send({
            "error": false,
            "message": "Konsen Api by GULA JAWA Team"
        });
    });

    app.get('ranking',userController.getRankingList);
    app.post('new',userController.createNewUser);
    app.put('update',userController.updatePointUser);
    app.get('user',userController.getDataUser);

};