/**
 * Created by muharizals on 07/04/2017.
 */

const User = require('../model/User'),
    Util = require('../util/util'),
    Constant = require('../util/Constant'),
    Helper = require('../helper/ResponseHelper');

    var io,listclient =[] ,numClients = {};;


exports.initSocketIo = function (Socket) {
  io = Socket;
  fistTimeSocket();
};


function emmitGameRoomToClient(data) {
    io.emit("gameroomreceiver",data);
}

function getMusuh(client) {
    let clindex = listclient.findIndex(item =>  item.id == client.id);
    if(clindex != -1){
        listclient[clindex].onrequest = true;
    }

    for (let i = 0; i < listclient.length; i++) {
       if(listclient[i].id != client.id){
            if(listclient[i].onwaiting && !listclient[i].onrequest){
                let gameroomname = "gr" + client.id + "and" + listclient[i].id;
                listclient[i].onwaiting = false;
                listclient[i].gameroom = gameroomname;
                listclient[clindex].onwaiting = false;
                listclient[clindex].gameroom = gameroomname;
                let datatosend = {
                  gameroom: gameroomname,
                  player1:client.id,
                  player2:listclient[i].id
                };

                listclient.splice(i, 1);
                let indextodelete = listclient.findIndex(item =>  item.id == client.id);
                listclient.splice(indextodelete, 1);
                console.log(listclient);

                emmitGameRoomToClient(datatosend);
                break;
            }
       }
    };

    let newcliindex = listclient.findIndex(item =>  item.id == client.id);
    if(newcliindex != -1){
        listclient[newcliindex].onrequest = false;
    }


}

function updateUserPointFromSocket(data) {
    if(!data.id || !data.point){
        return;
    }
    User.findOneAndUpdate({_id:data.id},{point:data.point},function (err,result) {

    });
}



function fistTimeSocket(){
    io.on('connection',function (socket) {
       console.log('new connection');

        let client = {
            id:socket.handshake.query.id,
            onwaiting:true,
            onrequest:true,
            gameroom:""
        };

       if(socket.handshake.query.id){
           let index = listclient.findIndex(item =>  item.id == socket.handshake.query.id);
           if(index == -1){
               listclient.push(client);
           }else{
               client = listclient[index];
           }

           getMusuh(client);

       }

       socket.on('update:point',function (data) {
            updateUserPointFromSocket(data);
       });

       socket.on('refresh:find',function (data) {
           console.log('refresh');
          getMusuh(client);
       });

       socket.on('join:room',function (data) {
            socket.join(data.gameroom,function () {
                console.log("join room " + data.gameroom);
                if (numClients[data.gameroom] == undefined) {
                    numClients[data.gameroom] = 1;
                } else {
                    numClients[data.gameroom]++;
                    if(numClients[data.gameroom] >= 2){
                        let tosend = {
                            code:"START_GAME"
                        };

                        io.to(data.gameroom).emit('message',tosend);
                    }
                }
                console.log(numClients[data.gameroom]);
            });
       });

       socket.on('leave:room',function (data) {
           socket.leave(data.gameroom,function () {
               console.log("leave room " + data.gameroom);
               if (numClients[data.gameroom] > 0) {
                   numClients[data.gameroom]--;
               }
               let tosend = {
                   code: "LEAVE_ROOM",
                   id:data.id
               };
               socket.in(data.gameroom).emit('message',tosend);
               console.log(numClients[data.gameroom]);
           });
       });

       socket.on('send:message',function (data) {
           console.log('pesan');
           console.log(data);
            socket.in(data.gameroom).emit('message',data);
       });


       socket.on('disconnect',function () {
           if(numClients[socket.room] > 0){
               numClients[socket.room]--;
           }
           let index = listclient.findIndex(item =>  item.id == socket.handshake.query.id);
           if(index != -1){
               listclient.splice(index, 1);
           }

           console.log("disconnect");
           console.log(listclient);
       });
    });

}

function findUser(body,res) {
    User.findOne({fbid:body.fbid},function (er_bos,cur_user) {
        if(er_bos){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }else{
            if(Util.isEmpty(cur_user)){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                return getRanking(cur_user,res);
            }
        }
    });
}


exports.createNewUser = function (req,res) {
      if(Util.validateParameter(req,["id","name","picture"])){
          let body = {
              fbid:req.params.id,
              name:req.params.name,
              picture:req.params.picture,
              point:0
          };
          if(req.params.hasOwnProperty("email")){
              body.email = req.params.email;
          }

                 let newUser = new User(body);
                 newUser.save(function (error) {
                    if(error){
                        try{
                            if(error.errors.hasOwnProperty("fbid") || error.errors.hasOwnProperty("email") ){
                                User.findOneAndUpdate({fbid:body.fbid},body,function (err_bos,cur_user) {
                                   if(err_bos){
                                       return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                                   }else{
                                       if(Util.isEmpty(cur_user)){
                                           return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                                       }else{
                                           return findUser(body,res);
                                       }
                                   }
                                });

                            }
                        }catch (error_2){
                            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                        }
                    }else{
                        return findUser(body,res);
                    }
                 });

      }else{
          return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false);
      }
};

function getRanking(user,res) {
    User.count({point:{"$gt":user.point}},function (err,count) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        else{
            user = user.toObject();
            user.ranking = count+1;
            return Helper.responseHelper(res,false,null,user);
        }
    });
}


exports.getDataUser = function(req,res){
    if(req.params.hasOwnProperty("id")){
        User.findOne({_id:req.params.id},function (err,user) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
				if(Util.isEmpty(user)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                 }
                return getRanking(user,res);
            }
        });
    }
    else if(req.params.hasOwnProperty("fbid")){
        User.findOne({fbid:req.params.fbid},function (err,user) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
					if(Util.isEmpty(user)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                 }
			 return getRanking(user,res);
            }
        });
    }
    else if(req.params.hasOwnProperty("email")){
        User.findOne({email:req.params.email},function (err,user) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
					if(Util.isEmpty(user)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                 }
                return getRanking(user,res);
            }
        });
    }
    else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false);
    }
};

exports.getRankingList = function (req,res) {
    let limit = 20;
    if(req.params.hasOwnProperty("limit")){
        limit =  parseInt(req.params.limit);
    }


    User.find({})
        .limit(limit)
        .sort('-point')
        .exec(function (err,data) {
            if(err){
                return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
            }else{
                data.forEach(function (item,index) {
                   item = item.toObject();
                   item.ranking = index+1;
                   data[index] = item;
                });
                return Helper.responseHelper(res,false,null,data);
            }
        });

};

function processRanking(user,point,res) {
    User.count({point:{"$gt":point}},function (err,count) {
        if(err){
            return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
        }
        else{
            user.point = point;
            user.save();
            user = user.toObject();
            user.ranking = count + 1;
            return Helper.responseHelper(res,false,null,user);
        }
    });
}

exports.updatePointUser = function(req,res){
    if(Util.validateParameter(req,["point"])){

        if(isNaN(req.params.point)){
            return Helper.responseHelper(res,true,Constant.INVALID_NUMBER);
        }

        if(req.params.hasOwnProperty("id")){
            User.findOne({_id:req.params.id},{},function (err,data) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }else{
                    if(Util.isEmpty(data)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }

                    return processRanking(data,req.params.point,res);
                }
            });
        }
        else if(req.params.hasOwnProperty("fbid")){
            User.findOne({fbid:req.params.fbid},function (err,data) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }else{
                    if(Util.isEmpty(data)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }
                    return processRanking(data,req.params.point,res);
                }
            });
        }
        else if(req.params.hasOwnProperty("email")){
            User.findOne({email:email},function (err,data) {
                if(err){
                    return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                }else{
                    if(Util.isEmpty(data)){
                        return Helper.responseHelper(res,true,Constant.UNKNOWN_ERROR);
                    }
                    return processRanking(data,req.params.point,res);
                }
            });
        }
        else{
            return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false);
        }
    }else{
        return Helper.responseHelper(res,true,Constant.PARAMETER_REQUIRED,false);
    }
};